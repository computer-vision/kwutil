from typing import Any
from typing import Dict
from _typeshed import Incomplete

TRUTHY_ENVIRONS: Incomplete


def envflag(key: str, default: Any | None = None, environ: None | Dict = None):
    ...
