kwutil.partial\_format module
=============================

.. automodule:: kwutil.partial_format
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
