"""
Script to help build the initial port
"""
import ubelt as ub
watch_repo = ub.Path('$HOME/code/watch').expand()
kwutil_repo = ub.Path('$HOME/code/kwutil').expand()
watch_modpath = watch_repo / 'watch'
watch_util_modpath = watch_modpath / 'utils'


kwutil_modpath = kwutil_repo / 'kwutil'
all_watch_utils = [p.name for p in watch_util_modpath.ls()]
print('all_watch_utils = {}'.format(ub.urepr(all_watch_utils, nl=1)))

portables = [
    'util_json.py',
    'util_progress.py',
    'util_resources.py',
    'util_time.py',
    'util_yaml.py',
    'util_parallel.py',
    'slugify_ext.py',
    'copy_manager.py',
    'partial_format.py',
    'util_pattern.py',
    'util_locks.py',
    'util_environ.py',
    'util_windows.py',
    'util_path.py',
    'util_eval.py',
]

# 'process_context.py',
# 'util_iter.py',
# 'util_dotdict.py',

new_fpaths = []
for name in portables:
    src = watch_util_modpath / name
    dst = kwutil_modpath / name
    src.copy(dst)

for fpath in new_fpaths:
    text = fpath.read_text()
    text = text.replace('watch.utils', 'kwutil')
    fpath.write_text(text)
