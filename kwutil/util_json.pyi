def debug_json_unserializable(data, msg: str = ...) -> None:
    ...


def ensure_json_serializable(dict_,
                             normalize_containers: bool = False,
                             verbose: int = ...):
    ...
