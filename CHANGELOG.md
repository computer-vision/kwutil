# Changelog

We are currently working on porting this changelog to the specifications in
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Version 0.3.4 - Unreleased

### Changes
* Most dependencies are now optional, which means you will likely need to depend on packages like ruamel.yaml, psutil, pint, and rich explicitly to get the most out of this package. But if you aren't using them, the package can now be installed without them.


* `JSON.dump` and `JSON.dumps` now support kwargs
* `JSON.ensure_serializable` now normalizes sets

### Added

* Add `util_path.sanitize_path_name`

## Version 0.3.3 - Released 2024-09-09

### Changes
* improvements to datetime class when working with pandas NaT.

### Added
* Add `kwutil.util_xml`
* Added low and high argument to `timedelta.random`
* Added `util_math.Rational`

## Version 0.3.2 - Released 2024-08-09

### Added
* port `util_rich` from kwcoco
* add `kwutil.filesystem_operation_manager` for generic CopyManager, DeleteManager,
  and MoveManager with parallel backends.
* Add `kwutil.Json`

### Changed
* Exposed more top-level attributes

## Version 0.3.1 - Released 2024-07-22

### Added
* Ported `util_hardware`, `util_units`, and `process_context` from geowatch.
* Ported `find_json_unserializable` from kwcoco.


## Version 0.3.0 - Released 2024-06-22

### Removed
* Drop support for 3.6 and 3.7

### Added
* Expose `timedelta` on the top level.
* Add `timedelta` methods: `to_pint` and `isoformat`.
* Added more extensions to `timedelta` and `datetime` classes.


## Version 0.2.5 - Released 2024-03-19

### Added
* Add `util_exception`
* Add more arguments to CopyManager submit
* Exposed a select few common attributes at the top level.


## Version 0.2.4 - Released 2023-11-12

### Fixed

* `coerce_timedelta` for numpy types
* `Yaml.coerce` no longer confuses directories.
* RichProgressManager now respects enabled=False and verbose=0


## Version 0.2.3 - Released 2023-07-21


## Version 0.2.2 - Released 2023-07-21


## Version 0.2.1 - Released 2023-07-21

### Added:
* `ensure_timezone` - new function for ensuring timezone objects

### Fixed:

* In `util_time.coerce_datetime`, integer timestamps now respects the default timezone (previously always used local time).


## [Version 0.2.0] -

### Changed
* Minor cleanups, and CI publishing.

## [Version 0.1.0] - Released 2023-06-11

### Added
* Initial version ported from geowatch
