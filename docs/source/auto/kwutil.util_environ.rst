kwutil.util\_environ module
===========================

.. automodule:: kwutil.util_environ
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
