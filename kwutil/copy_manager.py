"""
DEPRECATED.

CopyManager has moved to :mod:`fsops_managers`.
"""
from kwutil.fsops_managers import CopyManager
__all__ = ['CopyManager']
